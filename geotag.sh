#!/bin/bash

# Check for correct number of arguments
if (($# < 3)); then
  echo "ERROR: Incorrect number of arguments"
  echo "Provide in order the latitude, longitude, and full path to image"
  exit 1
fi

# Assign arguments to vars
LAT=$1
LON=$2
IMGS=( "${@:3}" )

# Check for exiftool
if ! hash exiftool 2>/dev/null; then
  echo "ERROR: exiftool not found, please install it"
  exit 1
fi

for img in "${IMGS[@]}"; do
  # Check that the given file exists
  if [[ ! -f $img ]]; then
      echo "ERROR: File '$img' not found"
      exit 1
  fi

  # Geotag the image
  exiftool "-GPSLatitude=$LAT" "-GPSLatitudeRef=$LAT" "-GPSLongitude=$LON" "-GPSLongitudeRef=$LON" "$img"
done