# Geotag

Simple Bash script for manually geotagging an image. Originally created as a
command for [gThumb](https://wiki.gnome.org/Apps/Gthumb), but it works
standalone as well.

## Install

```sh
git clone git@gitlab.com:austinbutler/geotag.git
sudo cp geotag/geotag.sh /usr/local/bin/geotag
```

### Add to gThumb (optional)

If you don't already have any custom commands, the following will work. You
can either use the `scripts.xml` file from this repo that has no keyboard
shortcut associated with it, or `scripts-shortcut.xml` which binds
`CRLT+SHIFT+G` as the shortcut to geotag.

```sh
cp geotag/scripts.xml ~/.config/gthumb/scripts.xml
```

However if you don't want to clobber any existing commands, you will need to 
manually edit `~/.config/gthumb/scripts.xml` to add the `<script>` in the 
`scripts.xml` in this repo.

## Run

```sh
geotag <latitude> <longitude> <filename>
```

### From gThumb

Click the wrench icon ("Tools") in the top-right, then "Geotag." It should
prompt you to input the latitude and longitude. After it's done hit enter to
open the image, then near the bottom-right click the "Map" icon and you should
see your coordinates!

If you used the shortcut file you can just press `CTRL+SHIFT+G` to start the 
geotag process.